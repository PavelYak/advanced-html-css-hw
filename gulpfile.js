const gulp = require('gulp'),
    sass = require('gulp-sass'),
    browserSync = require('browser-sync').create(),
    clean = require('gulp-clean'),
    imagemin = require('gulp-imagemin');

const cleanDist = () => {
    return gulp.src('dist', {read: false})
        .pipe(clean());
};

const scssBuild = () => {
    return gulp.src('src/scss/main.scss')
        .pipe(sass())
        .pipe(gulp.dest('dist/css/'));
};

const imagesBuild = () => {
    return gulp.src('src/img/*')
        .pipe(imagemin())

        .pipe(gulp.dest('dist/img/'))
};

const jsBuild = () => {
    return gulp.src('src/js/*.js')
        .pipe(gulp.dest('dist/js/'))
};

const watch = () => {
    gulp.watch('src/scss/*.scss', scssBuild).on('change', browserSync.reload);
    gulp.watch('src/js/*.js', jsBuild).on('change', browserSync.reload);
    gulp.watch('./index.html').on('change', browserSync.reload);
    gulp.watch('src/img/', imagesBuild).on('change', browserSync.reload);
    browserSync.init({
        server: {
            baseDir: "./"
        }
    })
};

gulp.task('dev', watch);
gulp.task('build', gulp.series(cleanDist, scssBuild, jsBuild, imagesBuild));